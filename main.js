const electron = require('electron')
var ipc = require('electron').ipcMain;
const { app, BrowserWindow, BrowserView, globalShortcut, session } = require('electron');
const jsonStorage = require('electron-json-storage');

let session1, session2 = session;
let browserView1, browserView2 = BrowserView;
const appUrl = 'https://web.whatsapp.com';

function createWindow () {

  const width = 1400; 
  const height = 700;
  let b1Active = true;

  browserWindow = new BrowserWindow({
    width: width,
    height: height,
    webPreferences: {
      nodeIntegration: true
    }
  });

  session1 = session.fromPartition('persist:session1', { cache: true });
  browserView1 = createBrowserView(appUrl, session1, width, height);
  loadCookieState('view1Cookies', session1);

  session2 = session.fromPartition('persist:session2', { cache: true });
  browserView2 = createBrowserView(appUrl, session2, width, height);
  loadCookieState('view2Cookies', session2);

  browserWindow.setBrowserView(browserView1);

  // use Ctrl+/ for windows and Command+/ for mac user
  globalShortcut.register('CommandOrControl+/', () => {
    b1Active ? browserWindow.setBrowserView(browserView2) : browserWindow.setBrowserView(browserView1);
    b1Active = !b1Active
  });

  browserWindow.loadFile('index.html')

  ipc.on('renderChat', function(event, tab){
    if(tab==1)
      browserWindow.setBrowserView(browserView1);
    else
      browserWindow.setBrowserView(browserView2);
  });
}

app.whenReady().then(createWindow)

app.on('window-all-closed', () => {

  if (process.platform !== 'darwin') {
    app.quit()
  }

  globalShortcut.unregisterAll();
  saveCookieState('view1Cookies', session1);
  saveCookieState('view2Cookies', session2);
  app.quit();

})

app.on('activate', () => {

  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow()
  }

})

function createBrowserView(url, session, width, height) {
  let browserView = new BrowserView({
    webPreferences: {
      nodeIntegration: false,
      nodeIntegrationInWorker: false,
      session: session
    }
  });
  browserView.setBounds({ x: 300, y: 0, width: 1200, height: height });
  browserView.webContents.loadURL(url, { userAgent: "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36"});
  return browserView;
}

function saveCookieState(sessionName, currentSession) {
  currentSession.cookies.get({}, (_, cookies) => {
    cookies.forEach(cookie => {
      const cDomain = !cookie.domain.startsWith('.') ? `.${cookie.domain}` : cookie.domain;
      cookie.url = `https://${cDomain}`
    });
    jsonStorage.set(sessionName, cookies, err => {
      if (err) {
        throw err;
      }
    });
  });
}

function loadCookieState(sessionName, currentSession) {
  jsonStorage.get(sessionName, (error, cookieData) => {
    if (Object.entries(cookieData).length === 0) {
      return;
    }
    if (error) {
      throw error;
    }
    cookieData.forEach(cookie => currentSession.cookies.set(cookie, error => {
      if (error) console.error(error);
    }));
  });
}
